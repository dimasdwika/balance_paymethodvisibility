<?php
class Balance_Paymethodvisibility_Block_Onepage_Payment_Methods extends Mage_Checkout_Block_Onepage_Payment_Methods
{
	public function getMethods()
    {
    	
		$methods = parent::getMethods();
		foreach ($methods as $key => $method) {
			if(!$this->isVisible($method)){
				unset($methods[$key]);
			}
		}
		return $methods;
    }


    public function isVisible($method)
    {
    	$visibility = Mage::getSingleton('balance_paymethodvisibility/visibility')->getVisibility( $method->getConfigData(Balance_Paymethodvisibility_Model_Visibility::CONFIG_NODE_VISIBILITY));
    	
    	return ($visibility !== Balance_Paymethodvisibility_Model_Visibility::VISIBILITY_ADMIN_ONLY);
    }

    public function getActivPaymentMethods()
    {
       $payments = Mage::getSingleton('payment/config')->getActiveMethods();
       $methods = array(array('value'=>'', 'label'=>Mage::helper('adminhtml')->__('--Please Select--')));
       foreach ($payments as $paymentCode=>$paymentModel) {
            $paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
            $methods[$paymentCode] = array(
                'label'   => $paymentTitle,
                'value' => $paymentCode,
            );
        }
        return $methods;
    }
}