<?php
class Balance_Paymethodvisibility_Model_Visibility {

	const VISIBILITY_ADMIN_FRONTEEND = 1;
	const VISIBILITY_ADMIN_ONLY = 2;
	const VISIBILITY_FRONTEND_ONLY = 3;

	const CONFIG_NODE_VISIBILITY = 'visibility';

	/**
	 * Options getter
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		return array(
				array('value' => self::VISIBILITY_ADMIN_FRONTEEND, 'label'=>Mage::helper('adminhtml')->__('Admin & Frontend')),
				array('value' => self::VISIBILITY_ADMIN_ONLY, 'label'=>Mage::helper('adminhtml')->__('Admin only')),
				array('value' => self::VISIBILITY_FRONTEND_ONLY, 'label'=>Mage::helper('adminhtml')->__('Front end only'))
		);
	}

	public function getVisibility($visiblity = 1){
		if ($visiblity == self::VISIBILITY_ADMIN_ONLY ){
			return self::VISIBILITY_ADMIN_ONLY;
		}
		if ($visiblity == self::VISIBILITY_FRONTEND_ONLY){
			return self::VISIBILITY_FRONTEND_ONLY;
		}
		return self::VISIBILITY_ADMIN_FRONTEEND;
	}
}